package models

import (
	"time"
)

type NewUser struct {
	Email     string `db:"email" json:"email"`
	Password  string `db:"password" json:"-"`
	FirstName string `db:"first_name" json:"firstName"`
	LastName  string `db:"last_name" json:"lastName"`
}

type User struct {
	ID        int       `db:"id, primarykey, autoincrement" gorm:"primary_key" json:"id"`
	Email     string    `db:"email" json:"email"`
	Password  string    `db:"password" json:"-"`
	FirstName string    `db:"first_name" json:"firstName"`
	LastName  string    `db:"last_name" json:"lastName"`
	UpdatedAt time.Time `db:"updated_at" json:"updatedAt"`
	CreatedAt time.Time `db:"created_at" json:"createdAt"`
}
