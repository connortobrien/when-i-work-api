package models

import "time"

type Shift struct {
	ID        int       `db:"id, primarykey, autoincrement" gorm:"primary_key" json:"id"`
	Start     time.Time `db:"start" json:"start" time_utc:"1"`
	End       time.Time `db:"end" json:"end" time_utc:"1"`
	UserID    int       `db:"user_id" json:"-"`
	User      User      `gorm:"foreignkey:user_id" json:"user"`
	UpdatedAt time.Time `db:"updated_at" json:"updatedAt"`
	CreatedAt time.Time `db:"created_at" json:"createdAt"`
}

type ShiftUpdate struct {
	Start *time.Time `db:"start" json:"start,omitempty"`
	End   *time.Time `db:"end" json:"end,omitempty"`
}

type NewShift struct {
	UserID int       `db:"user_id" json:"userId"`
	Start  time.Time `db:"start" json:"start" time_utc:"1"`
	End    time.Time `db:"end" json:"end" time_utc:"1"`
}
