package db

import (
	"fmt"
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/swift-panda/when-i-work/src/models"
)

var db *gorm.DB

const (
	DB_USER     = "postgres-dev"
	DB_PASSWORD = "zM9CV81HnCtg9KH3brzu"
	DB_NAME     = "when_i_work_dev"
)

func Init() {
	dbinfo := fmt.Sprintf("host=db port=5432 user=%s password=%s dbname=%s sslmode=disable", DB_USER, DB_PASSWORD, DB_NAME)
	var err error
	db, err = Connect(dbinfo)
	Migrate()
	if err != nil {
		log.Fatal(err)
	}
}

func Connect(dbinfo string) (*gorm.DB, error) {
	db, err := gorm.Open("postgres", dbinfo)
	if err != nil {
		panic(err)
	}
	return db, nil
}

func Migrate() {
	db.AutoMigrate(&models.User{}, &models.Shift{})
	db.Model(&models.Shift{}).AddForeignKey("user_id", "users(id)", "CASCADE", "CASCADE")
	fmt.Println("Auto Migration has been processed")
}

func Get() *gorm.DB {
	return db
}
