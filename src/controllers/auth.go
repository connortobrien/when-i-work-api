package controllers

import "github.com/gin-gonic/gin"

type AuthController struct{}

func (ctrl AuthController) Register(c *gin.Context) {
}

func (ctrl AuthController) Login(c *gin.Context) {
}

func (ctrl AuthController) Check(c *gin.Context) {
}
