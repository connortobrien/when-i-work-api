package controllers

import (
	"github.com/swift-panda/when-i-work/src/db"
	"github.com/swift-panda/when-i-work/src/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type Shift = models.Shift
type NewShift = models.NewShift

type ShiftController struct{}

func (ctrl ShiftController) Create(c *gin.Context) {
	db := db.Get()

	var newShift NewShift
	var user User
	if err := c.BindJSON(&newShift); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
	}
	if err := db.First(&user, newShift.UserID).Error; err != nil {
		c.AbortWithStatus(http.StatusConflict)
		return
	}

	shift := Shift{Start: newShift.Start, End: newShift.End, UserID: newShift.UserID}
	if valid := ValidateDates(shift.ID, shift.UserID, shift.Start, shift.End); valid == false {
		c.AbortWithStatus(http.StatusConflict)
		return
	}
	db.NewRecord(shift)
	db.Create(&shift)

	shift.User = user
	c.JSON(http.StatusCreated, &shift)
}

func (ctrl ShiftController) ReadAll(c *gin.Context) {
	db := db.Get()

	var shifts []Shift
	db.Preload("User").Find(&shifts)

	c.JSON(http.StatusOK, &shifts)
}

func (ctrl ShiftController) Get(c *gin.Context) {
	db := db.Get()

	var shift Shift
	if err := db.Preload("User").First(&shift, c.Param("id")).Error; err != nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	c.JSON(http.StatusOK, shift)
}

func (ctrl ShiftController) Put(c *gin.Context) {
	// TODO: Create or Update
}

func (ctrl ShiftController) Patch(c *gin.Context) {
	db := db.Get()

	var shift Shift
	var shiftUpdate models.ShiftUpdate
	if err := c.BindJSON(&shiftUpdate); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	if err := db.Preload("User").First(&shift, c.Param("id")).Error; err != nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	if shiftUpdate.Start != nil {
		shift.Start = *shiftUpdate.Start
	}
	if shiftUpdate.End != nil {
		shift.End = *shiftUpdate.End
	}
	if valid := ValidateDates(shift.ID, shift.UserID, shift.Start, shift.End); valid == false {
		c.AbortWithStatus(http.StatusConflict)
		return
	}

	db.Save(&shift)
	c.JSON(http.StatusOK, shift)
}

func (ctrl ShiftController) Delete(c *gin.Context) {
	db := db.Get()

	var shift Shift
	if err := db.First(&shift, c.Param("id")).Error; err != nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	db.Delete(&shift)
	c.Status(204)
}

func ValidateDates(shiftId int, userId int, start time.Time, end time.Time) bool {
	if start.Unix() > end.Unix() {
		return false
	}
	db := db.Get()
	var shifts []Shift
	query := db.Where("id != ? AND user_id = ?", shiftId, userId)
	query.Where(
		"start >= ? AND end <= ?", start, end,
	).Or(
		"start <= ? AND end >= ?", start, end,
	).Or(
		"start > ? AND start < ? AND end > ? AND end > ?", start, end, start, end,
	).Or(
		"start < ? AND start < ? AND end > ? AND end < ?", start, end, start, end,
	)
	query.Find(&shifts)
	if len(shifts) > 0 {
		return false
	}

	return true
}
