package controllers

import (
	"github.com/swift-panda/when-i-work/src/db"
	"github.com/swift-panda/when-i-work/src/models"

	"github.com/gin-gonic/gin"
)

type User = models.User

type UserController struct{}

func (ctrl UserController) Create(c *gin.Context) {
	db := db.Get()

	var user User
	if err := c.BindJSON(&user); err != nil {
		c.AbortWithStatus(400)
	}

	db.NewRecord(user)
	db.Create(&user)

	c.JSON(200, user)
}

func (ctrl UserController) ReadAll(c *gin.Context) {
	db := db.Get()

	var users []User
	db.Find(&users)

	c.JSON(200, users)
}

func (ctrl UserController) Get(c *gin.Context) {
	db := db.Get()

	var user User
	if err := db.First(&user, c.Param("id")).Error; err != nil {
		c.AbortWithStatus(404)
	}

	c.JSON(200, user)
}

func (ctrl UserController) Put(c *gin.Context) {
	// TODO: Create or Update
}

func (ctrl UserController) Patch(c *gin.Context) {
	db := db.Get()

	var user User
	if err := db.First(&user, c.Param("id")).Error; err != nil {
		c.AbortWithStatus(404)
		return
	}

	db.Update(&user)
	c.JSON(200, user)
}

func (ctrl UserController) Delete(c *gin.Context) {
	db := db.Get()

	var user User
	if err := db.First(&user, c.Param("id")).Error; err != nil {
		c.AbortWithStatus(404)
		return
	}

	db.Delete(&user)
	c.Status(204)
}
