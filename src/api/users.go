package api

import (
	"github.com/gin-gonic/gin"

	"github.com/swift-panda/when-i-work/src/controllers"
)

func ApplyUserRoutes(r *gin.RouterGroup) {
	user := new(controllers.UserController)

	users := r.Group("/users")
	{
		users.POST("/", user.Create)
		users.GET("/", user.ReadAll)
		users.GET("/:id", user.Get)
		users.PATCH("/:id", user.Patch)
		users.PUT("/:id", user.Put)
		users.DELETE("/:id", user.Delete)
	}
}
