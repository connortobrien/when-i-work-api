package api

import (
	"github.com/gin-gonic/gin"

	"github.com/swift-panda/when-i-work/src/controllers"
)

func ApplyShiftRoutes(r *gin.RouterGroup) {
	shift := new(controllers.ShiftController)

	shifts := r.Group("/shifts")
	{
		shifts.POST("/", shift.Create)
		shifts.GET("/", shift.ReadAll)
		shifts.GET("/:id", shift.Get)
		shifts.PATCH("/:id", shift.Patch)
		shifts.PUT("/:id", shift.Put)
		shifts.DELETE("/:id", shift.Delete)
	}
}
