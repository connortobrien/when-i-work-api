package api

import (
	"github.com/gin-gonic/gin"

	"github.com/swift-panda/when-i-work/src/controllers"
)

func ApplyAuthRoutes(r *gin.RouterGroup) {
	authCtrl := new(controllers.AuthController)

	auth := r.Group("/auth")
	{
		auth.POST("/register", authCtrl.Register)
		auth.POST("/login", authCtrl.Login)
		auth.GET("/check", authCtrl.Check)
	}
}
