FROM golang:1.12.6-alpine as builder

ENV GO111MODULE=on

RUN apk update && apk add --no-cache git ca-certificates

RUN mkdir app
WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

# RUN go get -d -v ./src/main.go

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main ./src/main.go

FROM scratch

WORKDIR /root/

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs
COPY --from=builder /app/main .

CMD ["./main"]
